FROM alpine:latest
RUN apk add --no-cache ansible openssh-client
CMD ["ansible","--help"]